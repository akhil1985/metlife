metApp.service('sharedProperties', function() {
    var indexVal = '';
    var userName = '';
    var quotes = [];

    return {
        getIndex: function() {
            return indexVal;
        },
        setIndex: function(value) {
            indexVal = value;
        },
        getUserName: function() {
            return userName;
        },
        setUserName: function(value) {
            userName = value;
        },
        getQuotes: function() {
            return quotes;
        },
        setQuotes: function(value) {
            quotes = value;
        }
    };
})